package banco;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;

import cliente.ICliente;
import solicitud.ISolicitudCredito;

import static org.mockito.Mockito.*;

class BancoTest {

	Banco banco;
	ICliente cliente;
	ISolicitudCredito solicitud;
	String descPropiedad = "";
	String dirPropiedad = "";
	double valorPropiedad = 0;
	double montoCreditoPersonal;
	double montoCreditoHipotecario;
	@BeforeEach
    void setUp() {
		banco = Banco.getSingletonBanco();
		cliente = mock(ICliente.class);
		solicitud = mock(ISolicitudCredito.class);
		montoCreditoPersonal = 5000;
		montoCreditoHipotecario = 2000000;
		descPropiedad = "";
		dirPropiedad = "";
		valorPropiedad = 2500000;
    }
	
	@DisplayName("Se agrega un nuevo cliente al banco")
    @Test 
    void agregarCliente() {
		banco.agregarCliente(cliente);
		assertTrue(banco.getClientes().size() > 0);
    }
	
	@DisplayName("Un cliente solicita un credito personal al banco")
    @Test 
    void solicitudDeCreditoPersonal() {
		banco.solicitarCreditoPersonal(cliente,montoCreditoPersonal,3);
		assertTrue(banco.getSolicitudes().size() > 0);
    }
	
	@DisplayName("Un cliente solicita un credito hipoteacario al banco")
    @Test 
    void solicitudDeCreditoHipotecario() {
		banco.solicitarCreditoHipotecario(cliente,montoCreditoHipotecario,240, descPropiedad, dirPropiedad, valorPropiedad);
		assertTrue(banco.getSolicitudes().size() > 0);
    }
	
	@DisplayName("Se acepto la solicitud de credito personal y se crea el credito personal")
    @Test 
    void seCreaElCreditoPersonal() {
		banco.crearCreditoPersonal(solicitud);
		assertTrue(banco.getCreditos().size() > 0);
    }
	
	@DisplayName("Se acepto la solicitud de credito hipotecario y se crea el credito hipotecario")
    @Test 
    void seCreaElCreditoHipotecario() {
		banco.crearCreditoHipotecario(solicitud);
		assertTrue(banco.getCreditos().size() > 0);
    }

}
