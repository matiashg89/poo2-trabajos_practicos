package solicitud;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;

import cliente.ICliente;

class SolicitudCreditoPersonalTest {

	SolicitudCreditoPersonal solicitudCredito;
	ICliente cliente;
	double monto = 0;
	int plazo = 0;
	@BeforeEach
    void setUp() {
		cliente = mock(ICliente.class);
		monto = 5000;
		plazo = 3;
		solicitudCredito = new SolicitudCreditoPersonal(cliente, monto, plazo);
	}
	
	@DisplayName("Se obtiene el monto de cada cuota")
    @Test 
    void obtenerMontoDeCuota() {		
		assertEquals(solicitudCredito.obtenerMontoCuota(),16667);
	}
	
	@DisplayName("Se valida si el credito es aceptable, y lo es")
    @Test 
    void esAceptable() {		
		solicitudCredito.esAceptable();
	}

}
