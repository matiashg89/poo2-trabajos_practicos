package propiedad;

public class PropiedadInmobiliaria implements IPropiedadInmobiliaria{

	private String descPropiedad;
	private String dirPropiedad; 
	private double valorPropiedad;
	
	public PropiedadInmobiliaria(String descPropiedad, String dirPropiedad, double valorPropiedad) {
		this.setDescPropiedad(descPropiedad);
		this.setDirPropiedad(dirPropiedad);
		this.setValorPropiedad(valorPropiedad);
	}

	public String getDescPropiedad() {
		return descPropiedad;
	}

	public void setDescPropiedad(String descPropiedad) {
		this.descPropiedad = descPropiedad;
	}

	public String getDirPropiedad() {
		return dirPropiedad;
	}

	public void setDirPropiedad(String dirPropiedad) {
		this.dirPropiedad = dirPropiedad;
	}

	public double getValorPropiedad() {
		return valorPropiedad;
	}

	public void setValorPropiedad(double valorPropiedad) {
		this.valorPropiedad = valorPropiedad;
	}
}
