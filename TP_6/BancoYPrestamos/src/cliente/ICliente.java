package cliente;

public interface ICliente {
	public int calcularEdad();
	public double calcularSueldoNetoAnual();
	public double getSueldoNetoMensual();
}
