package cliente;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;

public class Cliente implements ICliente{

	private LocalDate fecNac;
	private String apelNom; 
	private String direccion; 
	private double sueldoNetoMensual;

	public Cliente(String apelNom, String direccion, LocalDate fecNac, double sueldoNetoMensual) {
		this.setApelNom(apelNom);
		this.setDireccion(direccion);
		this.setFecNac(fecNac);
		this.setSueldoNetoMensual(sueldoNetoMensual);
	}

	public int calcularEdad() {
		Period edad = Period.between(this.getFecNac(), LocalDate.now());
		return edad.getYears();
	}

	private LocalDate getFecNac() {
		return this.fecNac;
	}

	public void setFecNac(LocalDate fecNac) {
		this.fecNac = fecNac;
	}

	public String getApelNom() {
		return apelNom;
	}

	public void setApelNom(String apelNom) {
		this.apelNom = apelNom;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public double getSueldoNetoMensual() {
		return sueldoNetoMensual;
	}

	public void setSueldoNetoMensual(double sueldoNetoMensual) {
		this.sueldoNetoMensual = sueldoNetoMensual;
	}

	public double calcularSueldoNetoAnual() {
		return this.getSueldoNetoMensual() * 12;
	}

}
