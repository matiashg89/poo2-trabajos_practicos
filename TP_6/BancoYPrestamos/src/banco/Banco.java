package banco;

import java.util.ArrayList;

import cliente.ICliente;
import credito.CreditoHipotecario;
import credito.CreditoPersonal;
import credito.ICredito;
import propiedad.IPropiedadInmobiliaria;
import propiedad.PropiedadInmobiliaria;
import solicitud.ISolicitudCredito;
import solicitud.SolicitudCreditoHipotecario;
import solicitud.SolicitudCreditoPersonal;

public class Banco {
	private static Banco singletonInstance;
	
	private ArrayList<ICliente> clientes;
	private ArrayList<ISolicitudCredito> solicitudes;
	private ArrayList<ICredito> creditos;
	
	private Banco() {
		clientes = new ArrayList<ICliente>();
		solicitudes = new ArrayList<ISolicitudCredito>();
		creditos = new ArrayList<ICredito>();
	}
	
	public static Banco getSingletonBanco() { 
        return singletonInstance == null ? singletonInstance = new Banco() : singletonInstance;
    }
	
	public void agregarCliente(ICliente cliente) {
		this.getClientes().add(cliente);
		
	}
	public ArrayList<ICliente> getClientes() {		
		return this.clientes;
	}
	public void solicitarCreditoPersonal(ICliente cliente, double montoCreditoPersonal, int plazo) {
		SolicitudCreditoPersonal solicitud = new SolicitudCreditoPersonal(cliente, montoCreditoPersonal,plazo);
		this.getSolicitudes().add(solicitud);		
	}
	public ArrayList<ISolicitudCredito> getSolicitudes() {
		return this.solicitudes;
	}
	public void solicitarCreditoHipotecario(ICliente cliente, double montoCreditoHipotecario, int plazo, String descPropiedad, String dirPropiedad, double valorPropiedad) {
		IPropiedadInmobiliaria propiedad = new PropiedadInmobiliaria(descPropiedad, dirPropiedad, valorPropiedad);
		SolicitudCreditoHipotecario solicitud = new SolicitudCreditoHipotecario(cliente, montoCreditoHipotecario,plazo,propiedad);
		this.getSolicitudes().add(solicitud);
	}
	public void crearCreditoPersonal(ISolicitudCredito solicitud) {
		CreditoPersonal credito = new CreditoPersonal(solicitud);
		this.getCreditos().add(credito);		
	}
	public ArrayList<ICredito> getCreditos() {
		return this.creditos;
	}
	public void crearCreditoHipotecario(ISolicitudCredito solicitud) {
		CreditoHipotecario credito = new CreditoHipotecario(solicitud);
		this.getCreditos().add(credito);
		
	}

}

