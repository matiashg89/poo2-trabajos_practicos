package credito;

import solicitud.ISolicitudCredito;

public abstract class Credito implements ICredito{
	private ISolicitudCredito solicitud;
	
	public Credito(ISolicitudCredito solicitud) {
		this.setSolicitud(solicitud);
	}

	public ISolicitudCredito getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(ISolicitudCredito solicitud) {
		this.solicitud = solicitud;
	}
}
