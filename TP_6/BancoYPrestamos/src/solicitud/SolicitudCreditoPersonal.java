package solicitud;

import banco.Banco;
import cliente.ICliente;

public class SolicitudCreditoPersonal extends SolicitudCredito{

	public SolicitudCreditoPersonal(ICliente cliente, double montoCreditoPersonal, int plazo) {
		super(cliente,montoCreditoPersonal,plazo);
	}

	public boolean esAceptable() {
		return this.getCliente().calcularSueldoNetoAnual() >= 15000 && 
			   this.obtenerMontoCuota() <= this.getCliente().getSueldoNetoMensual() * 0.7;	
	}
	
	public void aceptarSolicitud() {
		Banco.getSingletonBanco().crearCreditoPersonal(this);
	}

	

}
