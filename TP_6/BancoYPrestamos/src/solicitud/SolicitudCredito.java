package solicitud;

import cliente.ICliente;

public abstract class SolicitudCredito implements ISolicitudCredito{
	private ICliente cliente;
	private double montoCredito;
	private int plazo;
	
	public SolicitudCredito(ICliente cliente, double monto, int plazo) {
		this.setCliente(cliente);
		this.setmontoCredito(monto);
		this.setPlazo(plazo);
	}

	public ICliente getCliente() {
		return cliente;
	}

	public void setCliente(ICliente cliente) {
		this.cliente = cliente;
	}

	public double getmontoCredito() {
		return montoCredito;
	}

	public void setmontoCredito(double montoCredito) {
		this.montoCredito = montoCredito;
	}

	public int getPlazo() {
		return plazo;
	}

	public void setPlazo(int plazo) {
		this.plazo = plazo;
	}
	
	public Double obtenerMontoCuota() {
		return Math.ceil(this.getmontoCredito() / this.getPlazo());
	}
}
