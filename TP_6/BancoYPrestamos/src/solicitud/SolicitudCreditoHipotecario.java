package solicitud;
import java.time.LocalDate;
import banco.Banco;
import cliente.ICliente;
import propiedad.IPropiedadInmobiliaria;

public class SolicitudCreditoHipotecario extends SolicitudCredito{
	private IPropiedadInmobiliaria propiedad;
	
	public SolicitudCreditoHipotecario(ICliente cliente, double montoCreditoPersonal, int plazo,IPropiedadInmobiliaria propiedad) {
		super(cliente,montoCreditoPersonal,plazo);
		this.setPropiedad(propiedad);
	}
	public IPropiedadInmobiliaria getPropiedad() {
		return propiedad;
	}
	public void setPropiedad(IPropiedadInmobiliaria propiedad) {
		this.propiedad = propiedad;
	}
	
	public boolean esAceptable() {
		return this.getmontoCredito() <= this.getPropiedad().getValorPropiedad() * 0.7 && 
			   this.obtenerMontoCuota() <= this.getCliente().getSueldoNetoMensual() * 0.5 && 
			   this.getCliente().calcularEdad() + Math.ceil(this.getPlazo() / 12) <= 65;	
	}
	
	public void aceptarSolicitud() {
		Banco.getSingletonBanco().crearCreditoPersonal(this);
	}
}
